# Debearomatic

Automatically logs into hosts via SSH and supplies a given decryption secret so the hosts can continue booting.

# Environment

Debearomatic is intended to be used in conjunction with hosts running dropbear-initramfs on GNU+Linux but might also work elsewhere.

# Development status

Debearomatic is in early development. Hence, it is incomplete and lacks essential functionality as well as safety checks.

# License

Copyright (C) 2020-2021  Daniel Kulesz

Debearomatic is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License Version 3
as published by the Free Software Foundation on June 29, 2007.

Debearomatic is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Debearomatic.  If not, see <http://www.gnu.org/licenses/>.


# Build

Debearomatic is written in Rust and can be built using cargo just like other applications written in Rust.

## Sample instructions

Tested using live environments on:

- Debian GNU/Linux 10.7
- Devuan GNU+Linux 3.0
- Ubuntu 20.04.1 LTS (packages from `universe` need to be enabled)
- Alpine Linux 3.12.3 (packages from 'community' need to be enabled)

### Install dependencies

Debian/Devuan/Ubuntu:

    apt install git cargo libssl-dev pkg-config
    
Alpine:

    apk add git cargo openssl-dev pkgconf

### Clone and build

Then, as normal user, clone debearomatic:

    git clone https://gitlab.com/kuleszdl/debearomatic.git
    cd debearomatic
    
Select the release you want to build (replace `v0.2.1` by the desired version):
    
    git checkout v0.2.1
    
And start the build:
    
    cargo build --release

If everything went fine, you will find the resulting binary in the directory `target/release/debearomatic`.
As debearomatic is still in early development you might see various compiler warnings, but the build should succeed.


# Usage

Prerequisites:

- make sure ssh-agent knows your SSH identity (run `ssh-add -L` to check).
- make sure you have a SSH configuration file (in `$HOME/.ssh/config`).

Creating configuration:

Debearomatic will create a sample configuration file automatically when you run it for the first time:

    ./debearomatic
    
It will print where the file has been created. Just edit the file and run Debearomatic again.

Once you made sure debearomatic works, add it to your scheduler (e. g. cron) to automate the process.

