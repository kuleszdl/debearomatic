# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.2.1] - 2021-01-02

### Added
- Error handling for unreachable hosts
- Instructions for building and using the tool to the README


## [v0.2.0] - 2021-01-02

### Added
- Routines for creating an own json config file and reading from it

### Changed
- Replaced hardcoded values by values read from config files


## [v0.1.0] - 2021-01-01

### Added
- Implementation of first, very early but working hack using hardcoded values
- This CHANGELOG file
- License Information and headers

### Changed
- Updated copyright (added current year)
- Complete rewrite, now based on libssh2 (formerly on libssh)
