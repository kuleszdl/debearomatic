/**
 * This file is part of Debearomatic.
 * Copyright (C) 2020-2021  Daniel Kulesz
 *
 * Debearomatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation on June 29, 2007.
 *
 * Debearomatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Debearomatic.  If not, see <http://www.gnu.org/licenses/>.
 */

///
/// Data structure for SSH Hosts and logic for parsing the ssh config file
///
/// TODO: Parse the config only once and not once for each host
///

use crate::logic::own_configuration::ConfigurationItem;
use directories::UserDirs;
use ssh_config::SSHConfig;
use std::fs;
use std::path::PathBuf;


pub enum OperationMode {
    Waiting,
    Unknown,
}

pub struct SSHHost {
    description: String,
    host_alias: String,
    host: String,
    username: String,
    port: String, // we keep port as string because SSH library needs Strings as well
    secret: String,
}

impl SSHHost {
    // constructor
    pub fn new(in_configuration_item: ConfigurationItem) -> SSHHost {
        // Parse the config to retrieve host, port and username
        
        let x = parse_ssh_config(&in_configuration_item.host_alias);
        SSHHost {
            description: in_configuration_item.host_description,
            host_alias: in_configuration_item.host_alias,
            host: x[0].to_string(),
            port: x[1].to_string(),
            username: x[2].to_string(),
            secret: in_configuration_item.secret
        }
    }

    pub fn get_description(&self) -> &String {
        &self.description
    }

    pub fn get_alias_waiting(&self) -> &String {
        &self.host_alias
    }
    
    pub fn get_host(&self) -> &String {
        &self.host
    }
    
    pub fn get_username(&self) -> &String {
        &self.username
    }
    
    pub fn get_port(&self) -> &String {
        &self.port
    }
    
    pub fn get_secret(&self) -> &String {
        &self.secret
    }
    
    
}

/**
 * Parses the provided ssh config and returns a tuple/triple with host, port and username
 */
fn parse_ssh_config(host_alias: &str) -> [String; 3] {
    let filename = get_ssh_dir();
    let contents = fs::read_to_string(filename).unwrap();
    //    print!("{}", contents);
    let config = SSHConfig::parse_str(&*contents).unwrap();
    let host_settings = config.query(host_alias);
    let host = host_settings["Hostname"];
    let port = host_settings["Port"];
    let user = host_settings["User"];
    let results = [host.to_string(), port.to_string(), user.to_string()];
    return results;
}

/**
 * Computes and returns the full path to the SSH config file on the filesystem
 */
fn get_ssh_dir() -> String {
    let mut buf = String::with_capacity(0);

    if let Some(user_dirs) = UserDirs::new() {
        let mut path = PathBuf::from(user_dirs.home_dir());
        path.push(".ssh");
        path.push("config");
        let path_as_str = path.to_str().unwrap();
        buf.push_str(path_as_str);
    } else {
        // TODO this is not done properly, we don't want to return empty strings!
    }

    return buf;
}
