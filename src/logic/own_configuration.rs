use std::process;

use serde::{Deserialize, Serialize};
use serde_json::Result;
/**
 * This file is part of Debearomatic.
 * Copyright (C) 2020-2021  Daniel Kulesz
 *
 * Debearomatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation on June 29, 2007.
 *
 * Debearomatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Debearomatic.  If not, see <http://www.gnu.org/licenses/>.
 */

// This is needed to parse the json
use std::fs::File;

// We need these to read the config file
use directories::ProjectDirs;
use std::path::Path;
use std::path::PathBuf;

// Therese are for needed for creating a sample configuration file
use std::fs;
use std::io::prelude::*;

#[derive(Serialize, Deserialize, Clone)]
pub struct Configuration {
    pub hosts: Vec<ConfigurationItem>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ConfigurationItem {
    pub host_description: String,
    pub host_alias: String,
    pub secret: String,
}

impl Configuration {
    // constructor
    pub fn new() -> Configuration {
        // Parse the configfile
        let data = read_config();
        return data;
    }
}



fn read_config() -> Configuration {
    let configdir = get_or_create_config_dir();
    let data2 = get_or_create_config_file(&configdir);
    
    // Get the contents out of the json file
    let contents = fs::read_to_string(&data2).unwrap();

    // Parse the string of data into the structs
    let c: Configuration = serde_json::from_str(&contents).unwrap();

    // Do things just like with any other Rust data structure.
    println!("DEBUG: Found the following host aliases in configuration file:");
    for configitem in &c.hosts {
        println!("- {}", configitem.host_alias);
    }
    return c;
    
}

fn get_or_create_config_dir() -> String {
    let mut configpath = String::new();
    if let Some(proj_dirs) = ProjectDirs::from("me", "kulesz", "Debearomatic") {
        let dirpath = proj_dirs.config_dir();

        //
        if Path::new(dirpath).exists() {
            println!(
                "DEBUG: Found existing path for configuration files: {:?}",
                dirpath.to_str()
            );
        } else {
            println!("DEBUG: No existing path for configuration files found. Creating...");
            fs::create_dir_all(dirpath);
        }
        configpath.push_str(dirpath.to_str().unwrap());
    }
    return configpath;
}

fn get_or_create_config_file(configpath: &str) -> String {
    let mut path_to_config_file = String::new();
    // Construct the path to the file
    let mut p = PathBuf::new();
    p.push(configpath);
    p.push("debearomatic.json");
    if p.exists() {
        println!("DEBUG: Found main configuration file: {:?}", p.to_str());
        path_to_config_file.push_str(p.to_str().unwrap());
    } else { // TODO: Use directly json for serialization of the example
        println!("Error: No configuration file found! Is this your first run?");
        // create sample file
        let data = r#"
        {
            "hosts": [
                {
                    "host_description": "first sample host",
                    "host_alias": "host-one",
                    "secret": "the-very-secret-passphrase"                    
                },
                {
                    "host_description": "second sample host",
                    "host_alias": "host-two",
                    "secret": "yet-another-well-kept-secret"                    
                }
            ]
        }"#;
        let mut file = File::create(&p).unwrap();
        file.write_all(data.as_bytes());
        println!("Created sample configuration file: {:?}", p.to_str());
        println!("Please edit it and run this program again!");
        process::exit(1);
    }
    return path_to_config_file;
}
