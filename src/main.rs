/**
 * This file is part of Debearomatic.
 * Copyright (C) 2020-2021  Daniel Kulesz
 * 
 * Debearomatic is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation on June 29, 2007.
 * 
 * Debearomatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Debearomatic.  If not, see <http://www.gnu.org/licenses/>.
 */


//mod logic;
mod logic;

fn main() {
    print_copyright_notice();
//    logic::run();
    logic::run();
}

fn print_copyright_notice() {
    println!("Debearomatic  Copyright (C) 2020-2021  Daniel Kulesz");
    println!("This program comes with ABSOLUTELY NO WARRANTY; for details see the file 'LICENSE'.");
    println!("This is free software, and you are welcome to redistribute it");
    println!("under certain conditions; see the file 'LICENSE' for details.\n");
}