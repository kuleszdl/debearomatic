/**
 * This file is part of Debearomatic.
 * Copyright (C) 2020-2021  Daniel Kulesz
 *
 * Debearomatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 3
 * as published by the Free Software Foundation on June 29, 2007.
 *
 * Debearomatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Debearomatic.  If not, see <http://www.gnu.org/licenses/>.
 */
use ssh2::Session;
use std::io::prelude::*;
use std::net::TcpStream;

use std::io;

mod own_configuration;
mod sshhost;

pub fn run() {
    let config = own_configuration::Configuration::new();
    let mut ssh_hosts: Vec<sshhost::SSHHost> = vec![];
    for configitem in config.hosts.iter() {
        let clone = (*configitem).clone();
        let host = sshhost::SSHHost::new(clone);
        ssh_hosts.push(host);
    }
    print_sshhost_data(&ssh_hosts);

    println!("Starting processing...");

    for current_host in ssh_hosts.iter() {
        // Check in which the mode the host is
        // TODO: Implement this

        let op_mode = determine_operation_mode();
        match op_mode {
            sshhost::OperationMode::Unknown => {
                println!("Host is currently not waiting for decryption, nothing to do here.")
            }
            sshhost::OperationMode::Waiting => {
                match (connect_and_decrypt(current_host)) {
                    Ok(()) => (),
                    Error => {
                        // TODO: Verbose error would be much better
                        println!("Error: Connection failed!");
                    }
                }
            }
        }
    }

    println!("Finished processing.");
}

/**
 * Prints the data of the hosts to connect to, intended for debugging
 */
fn print_sshhost_data(in_testhosts: &Vec<sshhost::SSHHost>) {
    println!("Printing current configuration below:");

    let mut iter_count = 0;

    for current_host in in_testhosts.iter() {
        print_separator();
        println!("  Printing host #{}...", iter_count);
        println!("  Description: {}", current_host.get_description());
        println!(
            "  Host alias in SSH config file: {}",
            current_host.get_alias_waiting()
        );
        println!("  Port: {}", current_host.get_port());
        println!("  User: {}", current_host.get_username());
        println!(
            "  Secret present?: {} ",
            !current_host.get_secret().is_empty()
        );

        iter_count += 1;
    }
    print_separator();
}

/**
 * Prints a simple line separator that consts of a few dashes
 */
fn print_separator() {
    println!("  ----------------------");
}

/**
 * Check a supplied host and determine in which operation mode it is
 */
fn determine_operation_mode() -> sshhost::OperationMode {
    // TODO: Implement proper check; using static value for now
    return sshhost::OperationMode::Waiting;
}

fn connect_and_decrypt(in_ssh_host: &sshhost::SSHHost) -> Result<(), io::Error> {
    // Assemble String with host:port
    let mut host_and_port = String::new();
    host_and_port.push_str(in_ssh_host.get_host());
    host_and_port.push(':');
    host_and_port.push_str(in_ssh_host.get_port());

    println!("Connecting to host {}...", in_ssh_host.get_description());

    // Initialize the session and connect to the SSH server
    let tcp = TcpStream::connect(host_and_port)?;
    let mut sess = Session::new().unwrap();
    sess.set_tcp_stream(tcp);
    sess.handshake().unwrap();
    // Print the banner
    println!(
        "DEBUG: Connected. Remote sent this banner: {}",
        sess.banner().unwrap()
    );

    // Try to authenticate with the first identity in the agent.
    sess.userauth_agent(in_ssh_host.get_username()).unwrap();

    // Make sure session was authenticated
    assert!(sess.authenticated());
    println!("DEBUG: Authenticated.");
    // Open a channel
    let mut channel = sess.channel_session().unwrap();
    // Start a shell in this channel
    channel.shell().unwrap();
    // Read a few bytes (8), seems to be necessary before password can be entered.
    let mut buffer = [1u8; 8];
    let bytes_read = channel.read(&mut buffer[..]).unwrap();
    println!("DEBUG: Read the first {} bytes on the channel.", bytes_read);

    // Enter the secret (TODO: Instead of entering it blindly, check the welcome output from the remote first)
    channel.write(in_ssh_host.get_secret().as_bytes());
    println!("DEBUG: Sent secret.");
    // Make sure the host is not reachable anymore

    channel.wait_close();
    println!(
        "DEBUG: Channel exit status: {}",
        channel.exit_status().unwrap()
    );
    println!("DEBUG: Successfully terminated connection.");

    // Print last message for this iteration
    println!("DEBUG: Successfully finished processing this host.");
    Ok(())
}
